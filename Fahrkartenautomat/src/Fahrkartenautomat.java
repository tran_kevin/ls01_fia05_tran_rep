﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag = 0.0; 
       double eingezahlterGesamtbetrag = 0.0;
       double eingeworfeneMünze = 0.0;
       double rückgabebetrag = 0.0;
       double nochÜbrig = 0.0;
       byte AnzahlTickets = 0;

       System.out.print("Zu zahlender Betrag (EURO): ");
       zuZahlenderBetrag = tastatur.nextDouble();

       // Geldeinwurf
       // -----------
       System.out.print("Anzahl der Tickets: ");
       AnzahlTickets = tastatur.nextByte();   // Datentyp byte gewählt, da man für die Auswahl der Anzahl von Tickets nur Ganze Zahlen nimmt und es unwahrscheinlich ist, dass jemand +127 Tickets bestellt.
         
       zuZahlenderBetrag = zuZahlenderBetrag * (double) AnzahlTickets; // Hier habe ich den zuvor eingegebenen Betrag mal die Anzahl der Tickets gerechnet. Hierbei Anzahl Tickets als double, da Euro in 2 Nachkommastellen angezeigt wird.
       nochÜbrig = zuZahlenderBetrag; // Neue Variable für nochÜbrig um den neuen Wert von zuZahlenderBetrag beizubehalten
         
         while (eingezahlterGesamtbetrag < zuZahlenderBetrag) // Solange der eingezahlter Gesamtbetrag kleiner ist als der zu zahlender Betrag gibt er an wie viel noch zu Zahlen ist und man mehr Geld einwerfen muss.
         {
           System.out.printf("Noch zu zahlen: %.2f " + "Euro\n", nochÜbrig); // Ausdruck Anzeige wie viel nöch übrig zu Zahlen ist.
           System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): "); // Ausdruck Schrift zur Aufforderung zur Tastatureingabe für Weitere Münzen
             eingeworfeneMünze = tastatur.nextDouble(); // Weitere Münzeinwürfe
             eingezahlterGesamtbetrag += eingeworfeneMünze; // eingeworfene Münzen werden den Gesamtbetrag, dass schon eingezahlt wurde hinzuaddiert.
             nochÜbrig -= eingeworfeneMünze; // Was noch übrig zu zahlen ist wird subtrahiert von den Münzen die man einwirft.
         }
       

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
      Thread.sleep(250);
    } catch (InterruptedException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
       }
       System.out.println("\n\n");

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(rückgabebetrag > 0.0)
       {
         System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
         System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
            System.out.println("2 EURO");
            rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
            System.out.println("1 EURO");
            rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
            System.out.println("50 CENT");
            rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
            System.out.println("20 CENT");
            rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
            System.out.println("10 CENT");
            rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
            System.out.println("5 CENT");
            rückgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }
}