import java.util.Scanner;

class Fahrkartenautomat3
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag = 0.0; 
       double eingezahlterGesamtbetrag = 0.0;
       int Ausgabe;
       byte R�ckgeld;
       double r�ckgabebetrag = 0.0;

       // Fahrkartenbestellung erfassen
       // -----------------------------
       zuZahlenderBetrag = fahrkartenbestellungErfassen ();
       

       // Fahrkarte bezahlen
       // ------------------
    
       eingezahlterGesamtbetrag = fahrkartenBezahlen (zuZahlenderBetrag);
       

       // Fahrscheinausgabe
       // -----------------
       
       Ausgabe = fahrkartenAusgeben ();

       // R�ckgeldberechnung und -Ausgabe
       // -------------------------------
       
       R�ckgeld = r�ckgeldAusgeben (zuZahlenderBetrag, eingezahlterGesamtbetrag);

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
    }
    //Methode Fahrkartenbestellung Erfassen
    public static double fahrkartenbestellungErfassen ()
    {
    	Scanner tastatur = new Scanner(System.in);
    	
    	double zuZahlenderBetrag = 0.0;
    	byte AnzahlTickets = 0;
    	
    	System.out.print("Zu zahlender Betrag (EURO): ");
        zuZahlenderBetrag = tastatur.nextDouble();
        
        System.out.print("Anzahl der Tickets: ");
        AnzahlTickets = tastatur.nextByte();
        
        zuZahlenderBetrag = zuZahlenderBetrag * AnzahlTickets;
        		
        
        return zuZahlenderBetrag;
    }
    //Methode Fahrkarten bezahlen
    public static double fahrkartenBezahlen (double zuZahlenderBetrag)
    {
    	Scanner tastatur = new Scanner (System.in);
    	
    	double eingezahlterGesamtbetrag = 0.0;
    	double noch�brig = 0.0;
    	double eingeworfeneM�nze = 0.0;
    
    	noch�brig = zuZahlenderBetrag;
    
    while (eingezahlterGesamtbetrag < zuZahlenderBetrag)
	   {
		   System.out.printf("Noch zu zahlen: %.2f " + "Euro\n", noch�brig);
		   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
		   eingeworfeneM�nze = tastatur.nextDouble();
		   eingezahlterGesamtbetrag += eingeworfeneM�nze;
		   noch�brig -= eingeworfeneM�nze;
	   }
    return eingezahlterGesamtbetrag;
    }
    //Methode Fahrkarten ausgeben
    public static int fahrkartenAusgeben ()
    {
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
		return 0;
    }
    //Methode R�ckgeld ausgeben
    public static byte r�ckgeldAusgeben (double zuZahlenderBetrag, double eingezahlterGesamtbetrag) 
    {
    	double r�ckgabebetrag = 0.0;
    	
    	r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if(r�ckgabebetrag > 0.0)
        {
     	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f " + " Euro\n", r�ckgabebetrag);
     	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
            {
         	  System.out.println("2 EURO");
 	          r�ckgabebetrag -= 2.0;
            }
            while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
            {
         	  System.out.println("1 EURO");
 	          r�ckgabebetrag -= 1.0;
            }
            while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
            {
         	  System.out.println("50 CENT");
 	          r�ckgabebetrag -= 0.5;
            }
            while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
            {
         	  System.out.println("20 CENT");
  	          r�ckgabebetrag -= 0.2;
            }
            while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
            {
         	  System.out.println("10 CENT");
 	          r�ckgabebetrag -= 0.1;
            }
            while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
         	  System.out.println("5 CENT");
  	          r�ckgabebetrag -= 0.05;
            }
            while(r�ckgabebetrag >= 0.02)// 2 CENT-M�nzen
            {
         	  System.out.println("2 CENT");
  	          r�ckgabebetrag -= 0.02;
            }
            while(r�ckgabebetrag >= 0.01)// 1 CENT-M�nzen
            {
         	  System.out.println("1 CENT");
  	          r�ckgabebetrag -= 0.01;
            }
        }
    	return 0;
	}
}