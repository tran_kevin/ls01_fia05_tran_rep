import java.util.Scanner;

public class Addition 
{
	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		 double zahl1 = 0.0, zahl2 = 0.0, erg = 0.0;
		 int m;
		 double Zahleneingabe = 0.0;
		 double e = 0.0;
		 
		 	//1.Programmhinweis
	       
		 	m = programmHinweis();

	        //4.Eingabe
	        Zahleneingabe = eingabe(zahl1,zahl2);

	        //3.Verarbeitung
	        e = ergebnis(zahl1,zahl2,erg);

	        //2.Ausgabe
	        System.out.println("Ergebnis der Addition");
	        System.out.printf("%.2f = %.2f+%.2f", erg, zahl1, zahl2);
	}    
	        public static char programmHinweis()
	        {
	        	System.out.println("Hinweis: ");
	        	System.out.println("Das Programm addiert 2 eingegebene Zahlen. ");
	        
	        return 0;
	        }
	        public static double eingabe(double zahl1, double zahl2)
	        {
	        	System.out.println(" 1. Zahl:");
		        zahl1 = sc.nextDouble();
		        System.out.println(" 2. Zahl:");
		        zahl2 = sc.nextDouble();
		        
		    return 0;
	        }
	        public static double ergebnis(double zahl1, double zahl2, double erg)
	        {
	        erg = zahl1 + zahl2;
		        
		    return 0;
	        }
}